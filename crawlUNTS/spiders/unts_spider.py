import scrapy
from lxml import html
from datetime import datetime
import re, os

class UNTScrawler(scrapy.Spider):
    name = "unts"
    allowed_domains = ["treaties.un.org"]
    
    def __init__(self, urlfile=None, targetdir=None, *args, **kwargs):
        super(UNTScrawler, self).__init__(*args, **kwargs)
        if urlfile:
            try:
                with open(urlfile, "rt") as f:
                    self.start_urls = [url.strip() for url in f.readlines()]
            except OSError as e:
                print(f"{type(e)}: {e}")
        else:
            print("Please provide a file with urls to crawl, e.g. 'scrapy crawl unts -a urlfile=examples/urls.txt'")
        if targetdir:
            if os.path.exists(targetdir):
                self.targetdir = targetdir
                print(f"Data will be saved to {targetdir}. Existing files will be overwritten when fetching updates for them.")
            else:
                raise FileNotFoundError(f"{targetdir} directory does not exist.")
        else:
            self.targetdir = "examples/"
            print(f"Data will be saved to {self.targetdir}. Existing files will be overwritten when fetching updates for them.")
            
    def parse(self, response):
        # passing response body to lxml
        doc = html.fromstring(response.text)
        # pasting full urls into text fields
        els = doc.xpath('//div[@id="headerbox"]//a[contains(@href,"/")]|//div[@id="participants"]//a[@href]')
        domain = 'https://treaties.un.org'
        for el in els:
            el.text = el.text + ' [' + domain + el.get('href') + ']'
        # marking table headers with newlines
        for el in doc.xpath('//div[@id="headerbox"]//tr/th//*[text()]'): el.text = '\n' + el.text
        # extracting text content without whitespace
        htext = [x for x in doc.xpath('//div[@id="headerbox"]//tr//text()') if not x.isspace()]
        # concatenate into string
        hstr = '\t'.join(htext)
        # extracting treaty actions data
        ptext = doc.xpath('//div[@id="participants"]//text()')
        pstr = re.sub(r'(\t?\r\n\t*)+', r'\n', '\t'.join(ptext)).strip()
        # final join
        fullstr = '['+response.url+']\n['+str(datetime.utcnow().isoformat().split(".")[0]+'Z')+']\n\n'+hstr+'\n\n'+pstr
        # save as txt file
        filename = response.url.split("objid=")[-1] + '.txt'
        with open(self.targetdir + filename, 'w', encoding='utf-8') as f:
            try:
                f.write(fullstr)
            except OSError as e:
                print(f"{type(e)}: {e}")
            
