# UNTS crawler

This project provides a tool to efficiently and reproducibly gather treaty data for international law and international relations research.

## About the data
The United Nations Treaty Series [(UNTS)](https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml) is by far the largest and most authoritative collection of international agreements of the UN era. Under Article 102 of the [UN Charter](https://www.un.org/en/about-us/un-charter/full-text), all international agreements concluded by its member states must be registered with the UN Secretariat and be published by it.

UNTS is not to be confused with the collection of Multilateral Treaties Deposited with the Secretary-General [(MTDSG)](https://treaties.un.org/Pages/Content.aspx?path=DB/MTDSGStatus/pageIntro_en.xml) which is a small subset of UNTS, but sometimes contains additional notes and links to documents because the UN Secretary-General is depositary of these treaties. The Github repo [untreaties](https://github.com/zmjones/untreaties) has a set of scripts to gather MTDSG data (not the whole UN treaty collection as implied by the repo's description). Depositary and registrar roles are different. 

## Related projects
This tool is part of a pipeline, taking treaty page urls gathered by [unts-searchbot](https://gitlab.com/legalinformatics/unts-searchbot) (or otherwise saved to a file), visiting each page and saving the relevant data to a file. The reason for separating these two parts of the data gathering process is that historical index data is hardly ever updated whereas individual treaty pages should be updated whenever new parties join a multilateral agreement. Also, treaty/document page urls stay the same over time and these pages can be very efficiently scraped with Scrapy whereas using the treaty search engine requires a browser automation tool like [Selenium](https://www.selenium.dev/).

As for downstream uses of the data:
- [PILO](https://gitlab.com/legalinformatics/pilo) is a Public International Law Ontology in OWL2 format developed for the purpose of data integration
- [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) is a program for annotating UNTS treaty records and populating PILO with relevant data (not including texts from UNTS volumes in PDF)
- [Treaty-references](https://gitlab.com/martinakunz/treaty-references) uses the automatically retrieved UNTS data to generate a bibliographic database and other files for use in publications
- [Treaty-participation-maps](https://gitlab.com/legalinformatics/treaty-participation-maps) uses UNTS participation data retrieved from PILO for interactive treaty participation maps

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/

## Software requirements
- Operating system: I use Debian Linux and have not tested the bot on other systems, but it should be cross-platform (Windows users would need to modify file paths in the script from `/` to `\` I presume)
- Python: I use Python 3.9 but other versions >3.5 may work as well (see `requirements.txt` file)
- Python packages: See the `requirements.txt` file or `Pipfile.lock` for the exact versions I used in the last run, and `Pipfile` for minimal dependency specifications -- I recommend using [Pipenv](https://pipenv.pypa.io/) for Python dependency management when reproducible environments matter
  - `pipenv install --ignore-pipfile` installs packages based on `Pipfile.lock` which enables a deterministic build

# Reproducibility
To reproduce the bot from scratch on a new Debian or Ubuntu computer you could run:

```
sudo apt update && sudo apt install git python3.9 python3-pip
pip3 install --user pipenv==2023.10.3
git clone https://gitlab.com/legalinformatics/unts-crawler.git
cd unts-crawler
pipenv install --ignore-pipfile 
```
However, these software versions may already be outdated or have vulnerabilities. On a well-maintained operating system optimizing for safety and stability, I would simply install the latest version of [Scrapy](https://scrapy.org/) (e.g. `sudo apt install python3-scrapy` on Debian stable) and try running the bot with that. 

Also, the source data are likely to have changed in the meantime, so the output files will not be the same, but they should at least be similar. 

## How it works
The bot takes as input a text file with one url per line (see [`urls.txt`](https://gitlab.com/legalinformatics/unts-crawler/-/blob/main/examples/urls.txt) file in 'examples' folder) provided with a command line argument, e.g. `scrapy crawl unts -a urlfile=examples/urls.txt`, then visits those links, parses the pages and saves the output to a target directory which can also be specified on the commandline with `-a targetdir=/path/to/your/data/`. If no output folder is given, it will save the files to the examples folder.

The retrieved data is written to plain text files (one per treaty) because this is a much lighter format compared to html and because much of the metadata in these pages is not really in well-formatted html tables anyway. Columns are tab-separated (\t), which can be used in downstream processing to recover the structure. In the first two lines of the `.txt` file the source url and the time of last retrieval are added in square brackets. The file name consists in the identifying part of the treaty page url, including its language (English). The bot could be modified to retrieve data in French instead of or in addition to the English version.

The relevant urls embedded in the pages are inserted in square brackets immediately after the corresponding text element. This typically includes a link to the UNTS pdf volume in which the treaty appears, a link to the treaty text document (if available), and treaty action urls for each individual action (signature, ratification, reservation, objection etc). Following these links to gather more data could be a future extension of this project.

Other details of the process can be found in the [`unts_spider.py`](https://gitlab.com/legalinformatics/unts-crawler/-/blob/main/crawlUNTS/spiders/unts_spider.py) file and the [`settings.py`](https://gitlab.com/legalinformatics/unts-crawler/-/blob/main/crawlUNTS/settings.py) file, everything else is unmodified Scrapy boilerplate generated by its `scrapy startproject` command.

## Responsible use
Please DO NOT unnecessarily overload the UN servers! Only run the script if and when you really need it.
